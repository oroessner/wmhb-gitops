#!/usr/bin/env bash

architecture=$(arch)
image=djbasster/wmhb-gitops

if [ "$architecture" = "arm64" ] || [ "$CI" = true ]; then
    docker buildx build \
        --platform linux/arm64/v8,linux/amd64 \
        -t "${image}" \
        --push .
else
    docker build --no-cache --pull -t "${image}" .
    docker push "${image}"
fi
