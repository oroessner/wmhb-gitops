# wmhb-gitops

This is an example project, to showcase GitOps with portainer on [Webmontag I/2024 - 19.02.2024 - 18:30 Uhr](https://webmontag-bremen.de/events/webmontag-i-2024.html)

Slides: https://neusta-sd.slides.com/oroessner/gitops-mal-anders
